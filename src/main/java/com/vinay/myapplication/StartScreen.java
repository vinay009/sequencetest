package com.vinay.myapplication;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;



public class StartScreen extends Activity {

    public static final String SAVE_DATA = "SAVE_DATA" ;
    EditText getname;
    String prefs_data = "";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_screen);
        getname = (EditText) findViewById(R.id.name);
        SharedPreferences prefs = getSharedPreferences(StartScreen.SAVE_DATA, MODE_PRIVATE);
        prefs_data = prefs.getString("storename", "");

        if(!TextUtils.isEmpty(prefs_data)) {
            getname.setText("Welcome " + prefs_data);
        }
    }

    public void ebtn(View view) {
        if(prefs_data.equals("")){
       prefs();}
        Intent secondScreen = new Intent(this,MainActivity.class);
        startActivity(secondScreen);

    }

    public void hbtn(View view) {
        if(prefs_data.equals("")){
            prefs();}
        Intent secondScreen = new Intent(this,MainActivity.class);
        startActivity(secondScreen);

    }

    public void prefs(){
        SharedPreferences.Editor editor = getSharedPreferences(SAVE_DATA, MODE_PRIVATE).edit();
        editor.putString("storename", getname.getText().toString());
        editor.apply();
    }


}